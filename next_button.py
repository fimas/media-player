from my_button import MyButton
from tkinter import Widget


class NextButton(MyButton):
    def __init__(self, master: Widget | None = None, **kw) -> None:
        super().__init__(master, **kw)
        self.set_text("Next")
