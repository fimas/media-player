from tkinter import Canvas, Widget


class SongCanvas(Canvas):
    def __init__(self, master: Widget | None = None, **kw) -> None:
        super().__init__(master, **kw)
