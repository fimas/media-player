from typing import Callable, Dict
from enum import StrEnum


class EventID(StrEnum):
    MUSIC_START = "music_start"
    MUSIC_STOP = "music_stop"
    TRACK_CHANGE = "track_change"
    ADDED_TRACK = "added_track"
    EXIT = "exit"


class Event:
    """
    Represents an event that can be emitted and listened to by the
    EventManager. Each event has a name and associated data.

    Attrubutes:
            event_id (str): The name of the event.
            data (str): Event payload.
    """

    def __init__(self, event_id: EventID, data: str | None) -> None:
        """
        Initialize a new Event instance.

        Args:
            event_id (EventID): The name of the event (e.g., 'music_start').
            data (str): The data associated with the event. This can be any
                        relevant information that needs to be passed to the
                        listeners of the event.
        """

        self.event_id = event_id.value
        self.data = data


class EventManager:
    """
    Manages events and their listeners. Allows subscribing to events and
    emitting events, which will notify all subscribed listeners.

    Attributes:
            listeners (Dict[str, Callable[[Event], None]]): A dictionary of
                                                            event listeners
                                                            with the event
                                                            name as keys.
    """

    def __init__(self) -> None:
        """
        Initialize a new EventManager instance.
        """

        self.listeners: Dict[str, Callable[[Event], None]] = {}

    def subscribe(
            self,
            event_id: EventID,
            listener: Callable[[Event], None]) -> None:
        """
        Subscribe a listener to an event. The listener will be notified when
        the event is emitted.

        Args:
            event_name (str): The name of the event to subscribe to.
            listener (Callable[[Event], None]): A callable that will be called
                                                when the event is emitted. It
                                                must accept an Event instance
                                                as its only argument.

        Raises:
            ValueError: If the listener is not callable.
        """
        if not callable(listener):
            raise ValueError("Listener must be callable")

        if event_id.value not in self.listeners:
            self.listeners[event_id.value] = [listener]
        else:
            self.listeners[event_id.value].append(listener)

    def emit(self, event: Event) -> None:
        """
        Emit an event to all its subscribed listeners.

            event (Event): The Event instance to emit. All listeners subscribed
                           to this event's name will be notified and passed
                           this event instance.
        """
        if event.event_id in self.listeners:
            for listener in self.listeners[event.event_id]:
                listener(event)
