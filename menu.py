from tkinter import filedialog
from tkinter import messagebox
from tkinter import Menu
from tkinter import Widget
from playback_controller import PlaybackController


class MediaPlayerMenu(Menu):
    """
    A menu bar for the media player application.

    Attributes:
        playlist_view (PlaylistView): The playlist view to interact with.
        playlist (Playlist): The playlist model to update.
        eventmanager (EventManager): The event manager to emit events.
    """
    def __init__(self,
                 master: Widget | None,
                 playback_controller: PlaybackController,
                 **kw):
        super().__init__(master=master, **kw)
        self.playback_controller = playback_controller
        self.add_file_menu()
        master.configure(menu=self)

    def add_file_menu(self):
        """
        Adds a "File" menu to the menu bar with the following items:
        - Add to Playlist: Opens a file dialog to add a song to the playlist.
        - Exit: Exits the application.

        The "Add to Playlist" menu item should open a file dialog to select a
        song file to add to the playlist. The selected song should be added to
        the playlist view and the playlist model.

        The "Exit" menu item should exit the application.
        """
        file_menu = Menu(self, tearoff=0)
        file_menu.add_command(
            label="Add to Playlist",
            command=self.add_to_playlist)
        file_menu.add_command(label="Exit", command=self.exit_application)
        self.add_cascade(label="File", menu=file_menu)

    def add_to_playlist(self):
        """
        Opens a file dialog to select a song file to add to the playlist.
        The selected song is added to the playlist view and the playlist model.
        If no file is selected, an error message is displayed.
        """
        song_path = filedialog.askopenfilename(
                        filetypes=[("MP3 files", "*.mp3")])
        if song_path:
            self.playback_controller.add_song(song_path)
        else:
            messagebox.showinfo("Error", "No file selected.")

    def exit_application(self):
        # Events should only be emitted by the playback controller.
        self.playback_controller.exit_application()
