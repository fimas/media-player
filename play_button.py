from my_button import MyButton
from event_manager import Event
import pygame
from tkinter import Widget


class PlayButton(MyButton):
    """
    A specialized button that controls music playback. Inherits from MyButton
    for button functionality but adds specific behavior for playing music and
    responding to music-related events.
    """

    def __init__(self, master: Widget | None = None, **kw) -> None:
        """
        Initialize a new PlayButton instance.

        Args:
            master (Widget | None): The parent widget.
            **kw: Additional keyword arguments for MyButton.
        """
        super().__init__(master=master, **kw)
        self.set_text("Play")

    def music_start_listener(self, event: Event) -> None:
        """
        Listener to start music playback. It is intended to be subscribed to a
        music_start event.

        Args:
            event (Event): An Event instance with the 'data' attribute
                           containing the path to the music file.
        """
        sound = pygame.mixer.Sound(event.data)
        sound.play()
        self.forget_pos()

    def music_stop_listener(self, event: Event) -> None:
        """
        Listener to stop music playback. It is intended to be subscribed to a
        music_stop event. This method is designed to restore the Play button's
        position, making it visible and available for further interaction.

        Args:
            event (Event): An Event instance, not directly used in this method
                           but required for the listener interface.
        """
        self.restore_pos()
