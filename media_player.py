from tkinter import Tk
import pygame
from play_button import PlayButton
from stop_button import StopButton
from next_button import NextButton
from prev_button import PrevButton
from song_canvas import SongCanvas
from progress_bar import ProgressBar
from volume_slider import VolumeSlider
from playlist_view import PlaylistView
from menu import MediaPlayerMenu

from event_manager import EventManager, EventID, Event
from playback_controller import PlaybackController


class MediaPlayer(Tk):
    """
    A simple media player application built using Tkinter and pygame.

    This class initializes the media player's user interface, setting up
    playback controls, a song display canvas, a progress bar, and a volume
    slider. It also initializes the event management and playback control
    systems.

    Attributes:
        play_button (PlayButton): The button to start music playback.
        stop_button (StopButton): The button to stop music playback.
        next_button (NextButton): The button to skip to the next track.
        prev_button (PrevButton): The button to return to the previous track.
        song_canvas (SongCanvas): The display area for the current song's
                                  information.
        progress (ProgressBar): The progress bar for the current song.
        volume (VolumeSlider): The slider to adjust playback volume.
        playlist_view (PlaylistView): The view for the current playlist.
        event_manager (EventManager): Manages events for playback control.
        playback_controller (PlaybackController): Controls playback of songs.
    """

    def __init__(self, **kw) -> None:
        """
        Initializes the MediaPlayer window and its components.
        """
        super().__init__(**kw)
        self.title("FimAmp")
        self.geometry("400x200")

        pygame.init()
        pygame.mixer.init()

        self.initialize_ui()

    def initialize_ui(self) -> None:
        """
        Initializes the user interface components of the media player.

        Sets up the buttons, song canvas, progress bar, and volume slider,
        along with the event manager and playback controller.
        """
        self.event_manager = EventManager()
        self.playback_controller = PlaybackController(self.event_manager)

        self.menu = MediaPlayerMenu(
            master=self, 
            playback_controller=self.playback_controller)

        self.play_button = PlayButton(master=self, row=1, col=1)
        self.stop_button = StopButton(master=self, row=1, col=1)
        self.next_button = NextButton(master=self, row=1, col=3)
        self.prev_button = PrevButton(master=self, row=1, col=4)

        self.song_canvas = SongCanvas(self)

        self.progress = ProgressBar(self)
        self.volume = VolumeSlider(self)

        self.playlist_view = PlaylistView(
            master=self, 
            row=2, 
            col=1, 
            playlist=self.playback_controller.get_playlist(),
            eventmanager=self.event_manager,
            columnspan=4)
        
        self.bind_ui_callbacks()

    def bind_ui_callbacks(self) -> None:
        """
        Binds UI component callbacks to their respective functions and event
        listeners.

        Sets the command for the playback and stop buttons, and subscribes
        various UI components to listen to MUSIC_START and MUSIC_STOP events.
        """
        self.play_button.set_command(self.playback_controller.play)
        self.stop_button.set_command(self.playback_controller.stop)
        self.next_button.set_command(self.playback_controller.skip_track)

        self.event_manager.subscribe(
            EventID.MUSIC_START, 
            self.play_button.music_start_listener)
        
        self.event_manager.subscribe(
            EventID.MUSIC_START, 
            self.stop_button.music_start_listener)
        
        self.event_manager.subscribe(
            EventID.MUSIC_START, 
            self.playlist_view.select_track)

        self.event_manager.subscribe(
            EventID.MUSIC_STOP, 
            self.play_button.music_stop_listener)

        self.event_manager.subscribe(
            EventID.MUSIC_STOP, 
            self.stop_button.music_stop_listener)
        
        self.event_manager.subscribe(
            EventID.ADDED_TRACK, 
            self.playlist_view.on_track_added)
        
        self.event_manager.subscribe(
            EventID.EXIT,
            self.exit_application)
        
    def exit_application(self, event: Event) -> None:
        """
        Exits the application.

        Args:
            event (Event): The event that triggered the application exit.
        """
        self.destroy()
