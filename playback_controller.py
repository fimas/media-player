from event_manager import EventManager, Event, EventID
from playlist import Playlist
from song import Song


class PlaybackController:
    """
    Controls playback operations including play, stop, and track navigation.

    This controller uses an event manager to emit events related to music
    playback, such as starting or stopping a track. It manages a playlist
    of songs and can add songs to the playlist, play the current song, stop
    playback, skip to the next track, and handle the end of a track. Further
    the playback controller should be the only object to emit playback events.
    UI components should listen to these events and update accordingly.

    Attributes:
        eventmanager (EventManager): An instance of EventManager to emit
                                     playback events.
        playlist (Playlist): A Playlist instance to manage songs in the
                             playback queue.
    """

    def __init__(
            self,
            eventmanager: EventManager) -> None:
        """
        Initializes the PlaybackController with an EventManager instance.

        Args:
            eventmanager (EventManager): The event manager to use for emitting
                                         playback events.
        """
        self.eventmanager = eventmanager
        self.playlist = Playlist(self.eventmanager)

    def play(self) -> None:
        """
        Plays the current song in the playlist.

        Emits a MUSIC_START event with the path of the current song. If there's
        no song in the playlist, does nothing.
        """
        song = self.playlist.get_current()

        if song:
            self.eventmanager.emit(Event(EventID.MUSIC_START, data=song.path))
        else:
            pass

    def stop(self) -> None:
        """
        Stops the music playback.

        Emits a MUSIC_STOP event. The data attribute of the event is set to
        None as no specific song data is needed for stopping music playback.
        """
        self.eventmanager.emit(Event(EventID.MUSIC_STOP, data=None))

    def add_song(self, path: str) -> None:
        """
        Adds a new song to the playlist.

        Args:
            path (str): The file path to the song to be added.
        """
        self.playlist.add(Song(path))
        self.eventmanager.emit(Event(EventID.ADDED_TRACK, data=path))

    def skip_track(self) -> None:
        """
        Skips the current track and plays the next song in the playlist, if
        any.

        Marks the current track as finished, and if there is a next track,
        emits a MUSIC_START event with the path of the next song. If there is
        no next track, does nothing.
        """
        self.eventmanager.emit(Event(EventID.MUSIC_STOP, data=None))
        self.playlist.mark_current_track_finished()
        next_track = self.playlist.get_next()

        if next_track:
            self.eventmanager.emit(
                Event(EventID.MUSIC_START, data=next_track.path))
        else:
            pass

    def on_track_end(self) -> None:
        """
        Handles the end of a track by skipping to the next track in the
        playlist.

        Intended to be called when the current track finishes playing.
        """
        self.skip_track()

    def get_playlist(self):
        """
        Get the current playlist object.

        Returns:
            Playlist: The current playlist object.
        """
        return self.playlist

    def exit_application(self):
        """
        Stops the music playback and exits the application.
        """
        self.stop()
        self.eventmanager.emit(Event(EventID.EXIT, data=None))
