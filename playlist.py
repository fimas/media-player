from song import Song
from event_manager import EventManager, Event, EventID
import queue


class Playlist:
    """
    Manages a queue of songs to be played, keeping track of the current and
    next tracks.

    This class uses a queue to manage songs that have been added to the
    playlist, allowing for songs to be played in the order they were added. It
    also tracks the currently playing song.

    Attributes:
        queue (queue.Queue): A queue that holds songs to be played.
        current_track (Song | None): The currently playing song, or None if no
                                     song is playing.
        eventmanager (Eventmanager): An instance of the eventmanager so that
                                     this class can emit events.
        playlist_view (PlaylistView): An instance of the playlist view so that
                                      this class can update the displayed list.
    """
    def __init__(
            self,
            eventmanager: EventManager) -> None:
        """
        Initializes a new Playlist instance with an empty song queue and no
        current track.

        Args:
            eventmanager (Eventmanager): Eventmanager instance
            playlist_view (PlaylistView): Playlist view instance
        """
        self.queue = queue.Queue()
        self.current_track = None
        self.eventmanager = eventmanager

    def add(self, song: Song) -> None:
        """
        Adds a song to the playlist queue.

        Args:
            song (Song): The song to be added to the queue.
        """
        self.queue.put(song)

    def get_current(self) -> Song:
        """
        Returns the currently playing song.

        If there is no current track playing, it attempts to get the next
        track.

        Returns:
            The currently playing Song, or None if the playlist is empty.
        """
        if self.current_track:
            return self.current_track
        else:
            self.get_next()

    def get_next(self) -> Song:
        """
        Advances the playlist to the next song and returns it.

        If there is a current track, it is marked as finished. Then, the next
        song in the queue becomes the current track.

        Returns:
            The next Song in the queue, or None if the playlist is empty.
        """
        if self.current_track is not None:
            self.mark_current_track_finished()
        if not self.queue.empty():
            self.current_track = self.queue.get()

        return self.current_track

    def mark_current_track_finished(self) -> None:
        """
        Marks the current track as finished by setting the current track to
        None.
        """
        self.eventmanager.emit(Event(EventID.TRACK_CHANGE, data=None))
        self.current_track = None

    def get_queue_as_list(self):
        """
        Returns the queue as a list.

        Returns:
            List[Song]: A list of songs in the queue.
        """
        return list(self.queue.queue)

    def get_tracks(self):
        return self.get_queue_as_list()
