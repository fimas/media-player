from tkinter import Button, Widget
from typing import Tuple


class MyButton(Button):
    """
    A custom Button class that extends the Tkinter Button widget.
    This custom button allows for easy positioning within a grid layout,
    and offers convenience methods for position management and configuration.

    Attributes:
        row (int | None): The row position of the button.
        col (int | None): The column position of the button.
        padx (Tuple[int, int] | None): Horizontal padding.
        pady (Tuple[int, int] | None): Vertical padding.
    """

    def __init__(
            self,
            master: Widget | None = None,
            row: int | None = None,
            col: int | None = None,
            **kw) -> None:
        """
        Initialize a new MyButton instance.

        Args:
            master (Widget): The parent widget.
            row (int | None): The row position of the button in the grid.
            col (int | None): The column position of the button in the grid.
            **kw: Additional keyword arguments for the Button.
        """
        super().__init__(master, **kw)
        self.row = row
        self.column = col
        self.padx = (5, 5)
        self.pady = (5, 5)
        self.update_pos()

    def set_pos(
            self,
            row: int | None = None,
            col: int | None = None,
            padx: Tuple[int, int] | None = (5, 5),
            pady: Tuple[int, int] | None = (5, 5),
            **kw) -> None:
        """
        Set the position of the button in the grid.

        Args:
            row (int | None): The row position of the button.
            col (int | None): The column position of the button.
            padx (Tuple[int, int] | None): Horizontal padding.
            pady (Tuple[int, int] | None): Vertical padding.
            **kw: Additional grid options.
        """
        self.row = row
        self.column = col,
        self.padx = padx
        self.pady = pady
        self.update_pos(**kw)

    def update_pos(self, **kw) -> None:
        """
        Update the button's position in the grid using the current attributes.

        Args:
            **kw: Additional grid options.
        """
        self.grid(
            row=self.row,
            column=self.column,
            padx=self.padx,
            pady=self.pady,
            **kw)

    def forget_pos(self) -> None:
        """
        Temporarily remove the button from the grid layout.
        """
        self.grid_forget()

    def restore_pos(self) -> None:
        """
        Restore the button's position in the grid.
        """
        self.update_pos()

    def set_text(self, text: str) -> None:
        """
        Set the button's display text.

        Args:
            text (str): The text to display on the button.
        """
        self.configure(text=text)

    def set_command(self, command: callable) -> None:
        """
        Assign a command to be called when the button is clicked.

        Args:
            command (callable): A callable to be executed on button click.

        Raises:
            ValueError: If the provided command is not callable.
        """
        if not callable(command):
            raise ValueError("Command needs to be a callable")

        self.configure(command=command)
