from my_button import MyButton
from event_manager import Event
from tkinter import Widget
import pygame


class StopButton(MyButton):
    """
    A specialized button designed to stop music playback. Inherits from
    MyButton and adds specific behavior to interact with music-related events,
    namely stopping the music and managing its visibility based on the playback
    state.
    """

    def __init__(self, master: Widget | None = None, **kw) -> None:
        """
        Initialize a new StopButton instance.

        Args:
            master (Widget | None): The parent widget.
            **kw: Additional keyword arguments for MyButton.
        """
        super().__init__(master, **kw)
        self.set_text("Stop")
        self.forget_pos()

    def music_start_listener(self, event: Event) -> None:
        """
        Listener to respond to music start events. When music starts, this
        method makes the Stop button visible by restoring its position in the
        layout.

        Args:
            event (Event): An Event instance, not directly used in this method
                           but required for the listener interface. Represents
                           the event that music has started.
        """
        self.restore_pos()

    def music_stop_listener(self, event: Event) -> None:
        """
        Listener to respond to music stop events. Stops the music using
        pygame's mixer module and hides the Stop button from the layout, as
        it's no longer needed until music starts again.

        Args:
            event (Event): An Event instance, not directly used in this method
                           but required for the listener interface. Represents
                           the event that music has stopped.
        """
        pygame.mixer.stop()
        self.forget_pos()
