from tkinter import Listbox, Widget, END
from event_manager import EventManager, EventID, Event
from playlist import Playlist
from song import Song
from typing import List


class PlaylistView(Listbox):
    """
    A view component that displays a playlist of songs in a Listbox widget.

    Attributes:
        playlist (Playlist): The playlist object to display in the view.
        eventmanager (EventManager): The event manager to use for emitting
                                     events.
    """
    def __init__(
            self,
            master: Widget | None = None,
            row: int | None = None,
            col: int | None = None,
            playlist: Playlist | None = None,
            eventmanager: EventManager | None = None,
            columnspan: int | None = None,
            **kw):
        """
        Initializes the PlaylistView with a Playlist instance and an
        EventManager instance.

        Args:
            master (Widget): The parent widget of the PlaylistView.
            row (int): The row in the grid layout to place the PlaylistView.
            col (int): The column in the grid layout to place the PlaylistView.
            playlist (Playlist): The playlist object to display in the view.
            eventmanager (EventManager): The event manager to use for emitting
                                         events.
            **kw: Additional keyword arguments to pass to the Listbox
                  constructor.
        """
        super().__init__(master, selectmode='single', **kw)
        self.grid(row=row, column=col, columnspan=columnspan)
        self.playlist = playlist
        self.eventmanager = eventmanager

    def insert_items(self, items: List[Song]):
        """
        Insert a list of items into the playlist view, by clearing the current
        contents and inserting the new items.

        Args:
            items (List[Song]): A list of Song objects to insert into the
                                playlist view
        """
        self.delete(0, END)
        for item in items:
            self.insert(END, item.path)

    def on_track_added(self, event: Event):
        """
        Update the playlist view with the tracks currently in the playlist
        object

        Args:
            event (Event): The event object that triggered this function
        """
        self.insert_items(self.playlist.get_tracks())

    def dubble_click_track(self):
        """
        Emit a track change event when a track is double clicked in the
        playlist view to change the currently playing track in the media player
        view and start playback of the new track if the media player is
        currently stopped or paused and the new track is different from the
        currently playing track in the media player view. If the media player
        is currently playing a track, pause the current track and start
        playback of the new track.
        """
        self.eventmanager.emit(Event(EventID.TRACK_CHANGE))

    def get_selected_track(self):
        """
        Get the currently selected track in the playlist view

        Returns:
            str: The path of the currently selected track
        """
        selected_index = self.curselection()
        return self.get(selected_index)

    def select_track(self, track: Event):
        """
        Select a track in the playlist view by its path

        Args:
            track (Event): The event object containing the path of the track to
                           select in the playlist view as its data attribute
        """
        track_path = track.data
        track_index = self.get(0, END).index(track_path)
        self.selection_clear(0, END)
        self.selection_set(track_index)
        self.activate(track_index)
