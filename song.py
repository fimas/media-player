from mutagen.mp3 import MP3
from mutagen.easyid3 import EasyID3


class Song:
    """
    A representation of a song, including its metadata.

    This class encapsulates a song's file path and its metadata, such as title,
    artist, and album, read from the file's ID3 tags.

    Attributes:
        path (str): The file path to the song.
        title (str): The title of the song. Default is an empty string if not
                     found.
        artist (str): The artist of the song. Default is an empty string if not
                      found.
        album (str): The album of the song. Default is an empty string if not
                     found.
    """
    def __init__(self, path: str) -> None:
        """
        Initializes a Song instance with the given path and reads its metadata.

        Args:
            path (str): The file path of the song.
        """
        self.path = path
        self.title = ""
        self.artist = ""
        self.album = ""
        self.read_metadata()

    def read_metadata(self) -> None:
        """
        Reads the song's metadata using Mutagen.

        Attempts to extract the title, artist, and album from the song's ID3
        tags. If any of these tags are not present, their values remain as the
        default empty string.
        """
        try:
            audio = MP3(self.path, ID3=EasyID3)
            self.title = audio.get('title', [''])[0]
            self.artist = audio.get('artist', [''])[0]
            self.album = audio.get('album', [''])[0]
        except KeyError:
            pass
