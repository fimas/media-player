from tkinter import Scale, Widget


class ProgressBar(Scale):
    def __init__(self, master: Widget | None = None, **kw) -> None:
        super().__init__(master, **kw)
